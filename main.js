var electron = require("electron");
var app = electron.app;
var BrowserWindow = electron.BrowserWindow;
var ipcMain = electron.ipcMain;

var mainWindow;
var counter = 0;

console.log("starting!");

function createWindow() {
	mainWindow = new BrowserWindow({ width : 1024, height : 600});
	mainWindow.loadURL('file://'+__dirname+"/index.html");
	if (process.env.DEBUG === "true") { mainWindow.webContents.openDevTools(); }
	mainWindow.on('closed', function() {
		mainWindow = null;
	})

	mainWindow.webContents.on('did-finish-load', function() {
		setInterval(function () { mainWindow.webContents.send('bump-message', counter++) }, 100);
	})
}

app.on("ready", createWindow);

app.on('window-all-closed', function() {
	app.quit();
})
app.on("activate", function() {
	createWindow();
});

ipcMain.on('get-status', function(event, arg) {
	event.sender.send('new-status', 'THIS IS THE NEW STATUS?' + counter++);
	setTimeout(function() { event.sender.send('new-status', 'this was set via timeout')}, 2000);
});

ipcMain.on('nav-home', function(event, arg) {
	mainWindow.loadURL('file://'+__dirname+"/index.html");
});
ipcMain.on('nav-other', function(event, arg) {
	mainWindow.loadURL('file://'+__dirname+"/otherpage.html");
});
ipcMain.on('nav-swig', function(event, arg) {
	mainWindow.loadURL('file://'+__dirname+"/swig.html");
	ipcMain.once('render-ready', function(event, arg) {
		var options = { page : 'swig.swig', data : { pageTitle : 'this is fed from nav-swig' } };
		event.sender.send('render-page', options);
	});
})
