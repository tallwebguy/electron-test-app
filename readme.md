You'll need to have installed Electron with a

```
sudo npm install -g electron-prebuilt
```

and then run it with

```
npm start
```
