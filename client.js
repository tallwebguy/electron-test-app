var ipcRenderer = require('electron').ipcRenderer;
var paintStatus = function(event, status) {
	var newNotification = new Notification('status', { body : status });
	newNotification.onclick = function () {
		console.log("notification clicked");
	}
	document.getElementById('status').innerHTML = status ;
};
var paintBump = function(event, status) {
	document.getElementById('bump').innerHTML = status;
}
var getStatus = function() {
	console.log("asking for status");
	ipcRenderer.send("get-status",'something');
};
var navigate = function(target) {
	ipcRenderer.send('nav-'+target);
}
ipcRenderer.on('new-status', paintStatus);
ipcRenderer.on('bump-message', paintBump);
